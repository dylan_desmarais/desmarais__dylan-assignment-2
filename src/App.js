import React, { useState, useRef, useEffect } from "react";
import Form from "./components/Form";
import Todo from "./components/Todo";
import { nanoid } from "nanoid";


function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

function App(props) {
  const [tasks, setTasks] = useState(props.tasks);
  
  function deleteTask(id) {
    const remainingTasks = tasks.filter(task => id !== task.id);
    setTasks(remainingTasks);
  }
  
  function editTask(id, newName) {
    const editedTaskList = tasks.map(task => {
    // if this task has the same ID as the edited task
      if (id === task.id) {
        //
        return {...task, name: newName}
      }
      return task;
    });
    setTasks(editedTaskList);
  }

  const taskList = tasks
  .map(task => (
    <Todo
      id={task.id}
      name={task.name}
      completed={task.completed}
      key={task.id}
      deleteTask={deleteTask}
      editTask={editTask}
    />
  ));

  function addTask(name) {
    const newTask = { id: "task-" + nanoid(), name: name, completed: false };
    setTasks([...tasks, newTask]);
  }
  
  const tasksNoun = taskList.length !== 1 ? 'Tasks' : 'Task';
  const headingText = `${taskList.length} ${tasksNoun} Remaining`;
  
  const listHeadingRef = useRef(null);
  const prevTaskLength = usePrevious(tasks.length);
  
  useEffect(() => {
    if (tasks.length - prevTaskLength === -1) {
      listHeadingRef.current.focus();
    }
  }, [tasks.length, prevTaskLength]);
  
  return (
    <div className="task_app">
      <Form addTask={addTask} />
      <h2 tabIndex="-1" ref={listHeadingRef}>
        {headingText}
      </h2>
      <ul>
        {taskList}
      </ul>
    </div>
  );
}

export default App;