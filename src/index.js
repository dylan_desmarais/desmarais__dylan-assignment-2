import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';


const DATA = [
  { id: "task-0", name: "Task 1", completed: false },
  { id: "task-1", name: "Task 2", completed: false },
  { id: "task-2", name: "Task 3", completed: false }
];

ReactDOM.render(
  <React.StrictMode>
    <App tasks={DATA} />
  </React.StrictMode>,
  document.getElementById('root')
);