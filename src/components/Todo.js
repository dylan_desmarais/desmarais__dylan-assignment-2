import React, { useEffect, useRef, useState } from "react";


function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

export default function Task(props) {
  const [isEditing, setEditing] = useState(false);
  const [newName, setNewName] = useState('');

  const editFieldRef = useRef(null);
  const editButtonRef = useRef(null);

  const wasEditing = usePrevious(isEditing);

  function handleChange(e) {
    setNewName(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (!newName.trim()) {
      return;
    }
    props.editTask(props.id, newName);
    setNewName("");
    setEditing(false);
  }

  const editingTemplate = (
    <form onSubmit={handleSubmit}>
      <div>
        <label class="edit_warning" htmlFor={props.id}>
          New task name for {props.name}
        </label>
        <input
          id={props.id}
          className="edit_input_field"
          type="text"
          value={newName || props.name}
          onChange={handleChange}
          ref={editFieldRef}
        />
      </div>
      <div className="container_button">
      <button
          type="submit"
          className="button_save"
        >
          Save
        </button>
        <button
          type="button"
          className="button_cancel"
          onClick={() => setEditing(false)}
        >
          Cancel
        </button>
      </div>
    </form>
  );

  const viewTemplate = (
    <div>
      <div className="task_list">
        <input
          id={props.id}
          type="checkbox"
          defaultChecked={props.completed}
        />
        <label test htmlFor={props.id}>
          {props.name}
        </label>
      </div>
      <div className="container_button">
        <button
          type="button"
          className="button_edit"
          onClick={() => setEditing(true)}
          ref={editButtonRef}
        >
          Edit <span className="button_visually_hidden">{props.name}</span>
        </button>
        <button
            type="button"
            className="button_delete"
            onClick={() => props.deleteTask(props.id)}
        >
          Delete <span className="button_visually_hidden">{props.name}</span>
        </button>
      </div>
    </div>
  );

  useEffect(() => {
    if (!wasEditing && isEditing) {
      editFieldRef.current.focus();
    }
    if (wasEditing && !isEditing) {
      editButtonRef.current.focus();
    }
  }, [wasEditing, isEditing]);


  return <li>{isEditing ? editingTemplate : viewTemplate}</li>;
}
