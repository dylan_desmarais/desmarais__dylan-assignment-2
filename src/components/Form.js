import React, { useState } from "react";


function Form(props) {
  const [name, setName] = useState('');

  function handleSubmit(e) {
    e.preventDefault();
    if (!name.trim()) {
      return;
    }
    props.addTask(name);
    setName("");
  }

  function handleChange(e) {
    setName(e.target.value);
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2 className="title_container">
        <label htmlFor="new-todo-input" className="title_design">
          To-Do Task List
        </label>
      </h2>

      <input
        type="text"
        placeholder="Add A New Task..."
        className="input task_input_field"
        name="text"
        autoComplete="off"
        value={name}
        onChange={handleChange}
      />
      <button type="submit" className="button_add_task">
        Add
      </button>
    </form>
  );
}

export default Form;